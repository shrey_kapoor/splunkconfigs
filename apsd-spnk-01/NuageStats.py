#!/bin/python
import csv, json, string, sys, base64, time, re, urllib, urllib2, ssl
from time import gmtime, strftime, mktime
import subprocess
import requests
import logging
from requests.auth import HTTPDigestAuth
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from datetime import datetime

endTime = str(int(mktime(gmtime())))
startTime = str(int(mktime(gmtime()) - 60))
metricList = ["PACKETS_IN", "BYTES_IN", "PACKETS_IN_DROPPED", "PACKETS_IN_ERROR", "PACKETS_OUT", "BYTES_OUT PACKETS_OUT_DROPPED", "PACKETS_OUT_ERROR", "PACKETS_DROPPED_BY_RATE_LIMIT"]
class NuageStats:


    nuage_token = None

    def __init__(self):

        # disable annoying warnings
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        # Configure logging
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)

        # Nuage values
        self.nuage_org = 'csp'
        self.nuage_user = 'csproot'
        self.nuage_password = 'csproot'
        #self.nuage_url = "https://crt-vsd.itf.centurylink.net/nuage/api/v3_2"
        self.nuage_url  = "https://tst-vsd-01.tst.itf.centurylink.net:8443/nuage/api/v3_2"
        self.headers = {"Content-Type": "application/json",
                        "X-Nuage-Organization": "csp",
                        "Authorization": "Basic Y3Nwcm9vdDpjc3Byb290"}

    def get_nuage_token(self):
        """
        Calls the nuage /me function to get the nuage API key which is then hashed to generate
        an access token. User and passwords are hardcoded in the script using typical csproot org
        :return: Nuage API key
        """

        response = requests.get(self.nuage_url + "/me", headers=self.headers,
                                auth=HTTPDigestAuth(self.nuage_user, self.nuage_password), verify=False)
        if (response.status_code == requests.codes.OK):
            # parse out response
            data = response.json()
            self.nuage_token = data[0].get('APIKey')
        else:
            # If response code is not ok (200)
            self.logger.info("Error getting nuage token, using url ".format(self.nuage_url))
            response.raise_for_status()

    def get_nuage_management_enterprise(self):
        """
        Given a site id, return the enterprises
        :return: dictionary with enterprise name and enterprise ID
        """

        enterpriseList = []
        response = requests.get(self.nuage_url + "/enterprises", headers=self.headers,
                                auth=HTTPBasicAuth('csproot', self.nuage_token), verify=False)

        if (response.status_code == requests.codes.OK):
            # get the headers, which has the count returned
            h = response.headers
            count = h.get('X-Nuage-Count')
            data = response.json()
            for e in range(0, int(count)):
                ent = data[e].get('name')
                mgmt_enterprise = data[e].get('ID')
                enterpriseList.append({'name': ent, "ID": mgmt_enterprise})
            return enterpriseList
        else:
            self.logger.error("Could not find Nuage enterprises")
            response.raise_for_status()

    def get_nuage_management_domains_by_enterprise(self, enterpriseID):

        domain_list = []
        found = False

        response = requests.get(self.nuage_url + "/enterprises/" + enterpriseID + "/domains", headers=self.headers,
                                auth=HTTPBasicAuth('csproot', self.nuage_token), verify=False)

        if (response.status_code == requests.codes.OK):
            h = response.headers
            if response.headers.get('Content-Length') == '0':
                return domain_list
            count = h.get('X-Nuage-Count')
            self.logger.info("Found {} domains under the enterprise ".format(count))
            data = response.json()
            for e in range(0, int(count)):
                ent = data[e].get('name')
                domain_id = data[e].get('ID')
                domain_list.append({"name": ent, "ID": domain_id})
            return domain_list

        else:
            self.logger.error("Could not find Nuage domains  "
                              "with enterprise {}".format(enterpriseID))
            response.raise_for_status()

    def get_zones_by_domain(self, domainID):
        """
        :param domainID: UUID of the nuage domain we want to get the zones for
        :return: a list of dictionaries with the zone name and id
        """

        zone_list = []

        response = requests.get(self.nuage_url + "/domains/" + domainID + "/zones", headers=self.headers,
                                auth=HTTPBasicAuth('csproot', self.nuage_token), verify=False)

        if (response.status_code == requests.codes.OK):
            # get the response as JSON
            data = response.json()

            # get the headers, which has the count returned
            h = response.headers
            count = h.get('X-Nuage-Count')
            self.logger.info("Found {} zones under the domain {}".format(count, domainID))

            if count != '0':

                # loop throught the returned objects, we are expecting only 1?
                for i in range(0, int(count)):
                    myDict = {'name': data[i].get('name'),
                              'ID': data[i].get('ID'),
                              'publicZone': data[i].get('publicZone'),
                              }
                    zone_list.append(myDict)

                return zone_list
            else:
                self.logger.error(
                    "Could not find Nuage management domain for siteId {} and domainID {}".format(self.site, domainID))
                response.raise_for_status()

        else:
            self.logger.error(
                "Response code {} ; Could not find Nuage zones for domain {} ".format(response.status_code, domainID))
            response.raise_for_status()

    def get_zones_by_subnet(self, zoneId, zoneName):
        """
        Check a zone under the management subnet
        :param zoneId: UUID of zone to check subnet
        :param zoneName: friendly name of zone
        :return: None for not found or list of dictionaries
        """
        subnet_list = []

        response = requests.get(self.nuage_url + "/zones/" + zoneId + "/subnets", headers=self.headers,
                                auth=HTTPBasicAuth('csproot', self.nuage_token), verify=False)

        if (response.status_code == requests.codes.OK):

            h = response.headers
            count = h.get('X-Nuage-Count')
            self.logger.info("Found {} subnet(s) under the zone {}".format(count, zoneName))

            if count != '0':
                # get the response as JSON
                data = response.json()

                # loop throught the returned objects, we are expecting only 1?
                for i in range(0, int(count)):
                    myDict = {'name': data[i].get('name'), 'ID': data[i].get('ID'),}
                    subnet_list.append(myDict)

                return subnet_list

                self.logger.error("Could not find subnet for zone {}".format(zoneName))
                return None
            else:
                self.logger.error(
                    "Could not find Nuage subnets for domainID {}".format(zoneName))
                return None

        else:
            self.logger.error(
                "Response code {} ; Could not find Nuage management domainID {}".format(response.status_code, zoneName))
            response.raise_for_status()

    def get_vports_by_subnet(self, subnetId, subnetName):
        """
        Check for the ports under a subnet
        :param subnetId: UUID of subnet to check, usually management subnet
        :param subnetName: friendly name of subnet
        :return: None or list of dictionaries
        """
        vport_list = []

        response = requests.get(self.nuage_url + "/subnets/" + subnetId + "/vports", headers=self.headers,
                                auth=HTTPBasicAuth('csproot', self.nuage_token), verify=False)

        if (response.status_code == requests.codes.OK):

            # get the headers, which has the count returned
            h = response.headers
            count = h.get('X-Nuage-Count')
            self.logger.info("Found {} vports under the subnet {}".format(count, subnetName))

            if count != '0':

                # get the response as JSON
                data = response.json()

                # loop throught the returned objects, we are expecting only 1?
                for i in range(0, int(count)):
                    myDict = {'ID': data[i].get('ID'),
                              'parentType': data[i].get('parentType'),
                              'type': data[i].get('type'),}
                    vport_list.append(myDict)

                return vport_list
            else:
                self.logger.error(
                    "Could not find bridgeVports for subnet {}".format(subnetName))
                return None

        else:
            self.logger.error(
                "Response code {} ; Could not find Nuage management domain for  "
                "subnet {}".format(response.status_code, subnetName))

    def get_nuage_stats_by_vport(self, vPortId, metricType):

        response = requests.get(self.nuage_url + "/vports/" + vPortId + "/statistics" + "?startTime=" + startTime + "&endTime=" + endTime + "&metricTypes=" + 
					metricType + "&numberOfDataPoints=1", headers=self.headers,auth=HTTPBasicAuth('csproot', self.nuage_token), verify=False)

        if (response.status_code == requests.codes.OK):

            # get the headers, which has the count returned
            h = response.headers
            count = h.get('X-Nuage-Count')

            if count != '0':
                # get the response as JSON
                data = response.json()
                print data
    
    def timestamp():
	time_format = '%Y-%m-%d %H:%M:%S'
    	now = datetime.utcnow()
    	epoch = now.strftime('%s')
    	syslog_timestamp = now.strftime(time_format)
    	return epoch, syslog_timestamp

# ***************************************************************************
# Main is where the logic goes, basically calling various class functions
# that are intended to check on common items we have found the are typically
# not configured or installed correctly
# ***************************************************************************
if __name__ == '__main__':
    
    # First create an instance of this class
    # this calls the init method above and initializes the class
    myInstance = NuageStats()
    #epoch, syslog_timestamp = timestamp()
    # now get a token from nuage
    mytoken = myInstance.get_nuage_token()
    #epoch, syslog_timestamp = timestamp()
    # now get a list of enterprises
    enterpriseList = myInstance.get_nuage_management_enterprise()
    myInstance.logger.info("Found {} enterprises".format(len(enterpriseList)))
    #print "EnterPrise List============" 
    #print enterpriseList
    # now get the domains under each enterprise
    for i in range(0, len(enterpriseList)):
        domainList = myInstance.get_nuage_management_domains_by_enterprise(enterpriseList[i].get('ID'))
        for j in range(0, len(domainList)):
            myInstance.logger.info("Enterprise {} has domain name={} ID={}".format(enterpriseList[i].get('name'),
                                                                                   domainList[j].get('name'),
                                                                                   domainList[j].get('ID')))
	    # Now get the zones for each domain
            zoneList = myInstance.get_zones_by_domain(domainList[j].get('ID'))
            # Now for each zone get the subnet
            for k in range(0, len(zoneList)):
                subnetList = myInstance.get_zones_by_subnet(zoneList[k].get('ID'), zoneList[k].get('name'))
                if subnetList:
                    for l in range(0, len(subnetList)):
                        vportList = myInstance.get_vports_by_subnet(subnetList[l].get('ID'), subnetList[l].get('name'))
                        if vportList:
                            for m in range(0, len(vportList)):
                                myInstance.logger.info(
                                    " \n*** Found vport {} with id {}".format(vportList[m].get('type'),
                                                                              vportList[m].get('ID')))
          			#print vportList[m].get('ID')
				for n in range(0, len(metricList)):
				    print "{} getting stats for enterprise {} vport {} subnet {} zone {} domain {}".format(
				    endTime,
				    enterpriseList[i].get('name'),
                                    vportList[m].get('type'),
                                    subnetList[l].get('name'),
                                    zoneList[k].get('name'),
                                    domainList[j].get('name'))
                                    myInstance.get_nuage_stats_by_vport(vportList[m].get('ID'),metricList[n])

